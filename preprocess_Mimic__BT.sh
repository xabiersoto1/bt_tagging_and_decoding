#Variable definition, replace your_x_dir by your own directories.
MIMIC_DIR=your_mimic_dir
UFAL_DIR=your_ufal_dir
SUBWORD_NMT_DIR=your_subword_nmt_dir
TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR=your_training_with_unrestricted_sampling_dir
TRAINING_WITH_TAGGED_UNRESTRICTED_SAMPLING_DIR=your_training_with_tagged_unrestricted_sampling_dir
TRAINING_WITH_BEAM_SEARCH_DIR=your_training_with_unrestricted_sampling_dir
TRAINING_WITH_TAGGED_BEAM_SEARCH_DIR=your_training_with_tagged_unrestricted_sampling_dir
TRAINING_WITH_RESTRICTED_SAMPLING_DIR=your_training_with_unrestricted_sampling_dir
TRAINING_WITH_TAGGED_RESTRICTED_SAMPLING_DIR=your_training_with_tagged_unrestricted_sampling_dir

cd $MIMIC_DIR

sed -r 's/(@@ )|(@@ ?$)//g' < mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_unrestricted_sampling.bpe.de > mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_unrestricted_sampling.tc.de

sed -r 's/(@@ )|(@@ ?$)//g' < mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.bpe.en > mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.tc.en

#Prepare the training data using the MIMIC corpus translated through unrestricted sampling
cd $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR

cat $UFAL_DIR/ufal_medical_without_subtitles.tc.de $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_unrestricted_sampling.tc.de > train_unshuffled.tc.de
cat $UFAL_DIR/ufal_medical_without_subtitles.tc.en $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.tc.en > train_unshuffled.tc.en

paste -d'\t' train_unshuffled.tc.de train_unshuffled.tc.en > train_unshuffled.tc.bi

shuf train_unshuffled.tc.bi > train.tc.bi

cut -f 1 train.tc.bi > train.tc.de
cut -f 2 train.tc.bi > train.tc.en

rm -f train_unshuffled*
rm -f train.tc.bi

$YOUR_SUBWORD_NMT_DIR/learn_joint_bpe_and_vocab.py --input train.tc.de train.tc.en -s 40000 -o bpe_model.bi --write-vocabulary vocab.de vocab.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < train.tc.de > train.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < train.tc.en > train.bpe.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < $KHRESMOI_DIR/khresmoi-summary-dev.tc.de > khresmoi-summary-dev.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < $KHRESMOI_DIR/khresmoi-summary-dev.tc.en > khresmoi-summary-dev.bpe.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < $KHRESMOI_DIR/khresmoi-summary-test.tc.de > khresmoi-summary-test.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < $KHRESMOI_DIR/khresmoi-summary-test.tc.en > khresmoi-summary-test.bpe.en

#Prepare the training data using the MIMIC corpus translated through tagged unrestricted sampling
sed -e 's/^/<BT> /' $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_unrestricted_sampling.tc.de > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_unrestricted_sampling_tagged.tc.de

cd $TRAINING_WITH_TAGGED_UNRESTRICTED_SAMPLING_DIR

cat $UFAL_DIR/ufal_medical_without_subtitles.tc.de $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_unrestricted_sampling_tagged.tc.de > train_unshuffled.tc.de
cat $UFAL_DIR/ufal_medical_without_subtitles.tc.en $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.tc.en > train_unshuffled.tc.en

paste -d'\t' train_unshuffled.tc.de train_unshuffled.tc.en > train_unshuffled.tc.bi

shuf train_unshuffled.tc.bi > train.tc.bi

cut -f 1 train.tc.bi > train.tc.de
cut -f 2 train.tc.bi > train.tc.en

rm -f train_unshuffled*
rm -f train.tc.bi

#This time you should use the same bpe model and vocabulary learned with the untagged corpus, adding the '--glossaries' argument for not segmenting the '<BT>' tag in the source language. The dev and test corpora are also the same learned before with the untagged corpus, so there is no need to create them again.
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/bpe_model.bi --glossaries '<BT>' --vocabulary $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/vocab.de < train.tc.de > train.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/bpe_model.bi --vocabulary $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/vocab.en < train.tc.en > train.bpe.en

cp $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/khresmoi-summary-dev.bpe.de khresmoi-summary-dev.bpe.de
cp $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/khresmoi-summary-dev.bpe.en khresmoi-summary-dev.bpe.en
cp $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/khresmoi-summary-test.bpe.de khresmoi-summary-test.bpe.de
cp $TRAINING_WITH_UNRESTRICTED_SAMPLING_DIR/khresmoi-summary-test.bpe.en khresmoi-summary-test.bpe.en

#Prepare the training data using the MIMIC corpus translated through beam search
sed -r 's/(@@ )|(@@ ?$)//g' < $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_beam_search_16.bpe.de > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_beam_search_16.tc.de

cd $TRAINING_WITH_BEAM_SEARCH_DIR

cat $UFAL_DIR/ufal_medical_without_subtitles.tc.de $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_beam_search_16.tc.de > train_unshuffled.tc.de
cat $UFAL_DIR/ufal_medical_without_subtitles.tc.en $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.tc.en > train_unshuffled.tc.en

paste -d'\t' train_unshuffled.tc.de train_unshuffled.tc.en > train_unshuffled.tc.bi

shuf train_unshuffled.tc.bi > train.tc.bi

cut -f 1 train.tc.bi > train.tc.de
cut -f 2 train.tc.bi > train.tc.en

rm -f train_unshuffled*
rm -f train.tc.bi

$YOUR_SUBWORD_NMT_DIR/learn_joint_bpe_and_vocab.py --input train.tc.de train.tc.en -s 40000 -o bpe_model.bi --write-vocabulary vocab.de vocab.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < train.tc.de > train.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < train.tc.en > train.bpe.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < khresmoi-summary-dev.tc.de > khresmoi-summary-dev.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < khresmoi-summary-dev.tc.en > khresmoi-summary-dev.bpe.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < khresmoi-summary-test.tc.de > khresmoi-summary-test.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < khresmoi-summary-test.tc.en > khresmoi-summary-test.bpe.en

#Prepare the training data using the MIMIC corpus translated through tagged beam search
sed -e 's/^/<BT> /' $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_beam_search_16.tc.de > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_beam_search_16_tagged.tc.de

cd $TRAINING_WITH_TAGGED_BEAM_SEARCH_DIR

cat $UFAL_DIR/ufal_medical_without_subtitles.tc.de $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_beam_search_16_tagged.tc.de > train_unshuffled.tc.de
cat $UFAL_DIR/ufal_medical_without_subtitles.tc.en $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.tc.en > train_unshuffled.tc.en

paste -d'\t' train_unshuffled.tc.de train_unshuffled.tc.en > train_unshuffled.tc.bi

shuf train_unshuffled.tc.bi > train.tc.bi

cut -f 1 train.tc.bi > train.tc.de
cut -f 2 train.tc.bi > train.tc.en

rm -f train_unshuffled*
rm -f train.tc.bi

#This time you should use the same bpe model and vocabulary learned with the untagged corpus, adding the '--glossaries' argument for not segmenting the '<BT>' tag in the source language. The dev and test corpora are also the same learned before with the untagged corpus, so there is no need to create them again.
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c $TRAINING_WITH_BEAM_SEARCH_DIR/bpe_model.bi --glossaries '<BT>' --vocabulary $TRAINING_WITH_BEAM_SEARCH_DIR/vocab.de < train.tc.de > train.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c $TRAINING_WITH_BEAM_SEARCH_DIR/bpe_model.bi --vocabulary $TRAINING_WITH_BEAM_SEARCH_DIR/vocab.en < train.tc.en > train.bpe.en

cp $TRAINING_WITH_BEAM_SEARCH_DIR/khresmoi-summary-dev.bpe.de khresmoi-summary-dev.bpe.de
cp $TRAINING_WITH_BEAM_SEARCH_DIR/khresmoi-summary-dev.bpe.en khresmoi-summary-dev.bpe.en
cp $TRAINING_WITH_BEAM_SEARCH_DIR/khresmoi-summary-test.bpe.de khresmoi-summary-test.bpe.de
cp $TRAINING_WITH_BEAM_SEARCH_DIR/khresmoi-summary-test.bpe.en khresmoi-summary-test.bpe.en

#Prepare the training data using the MIMIC corpus translated through restricted sampling
sed -r 's/(@@ )|(@@ ?$)//g' < $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_restricted_sampling_50.bpe.de > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_restricted_sampling_50.tc.de

cd $TRAINING_WITH_RESTRICTED_SAMPLING_DIR

cat $UFAL_DIR/ufal_medical_without_subtitles.tc.de $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_restricted_sampling_50.tc.de > train_unshuffled.tc.de
cat $UFAL_DIR/ufal_medical_without_subtitles.tc.en $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.tc.en > train_unshuffled.tc.en

paste -d'\t' train_unshuffled.tc.de train_unshuffled.tc.en > train_unshuffled.tc.bi

shuf train_unshuffled.tc.bi > train.tc.bi

cut -f 1 train.tc.bi > train.tc.de
cut -f 2 train.tc.bi > train.tc.en

rm -f train_unshuffled*
rm -f train.tc.bi

$YOUR_SUBWORD_NMT_DIR/learn_joint_bpe_and_vocab.py --input train.tc.de train.tc.en -s 40000 -o bpe_model.bi --write-vocabulary vocab.de vocab.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < train.tc.de > train.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < train.tc.en > train.bpe.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < khresmoi-summary-dev.tc.de > khresmoi-summary-dev.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < khresmoi-summary-dev.tc.en > khresmoi-summary-dev.bpe.en

$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < khresmoi-summary-test.tc.de > khresmoi-summary-test.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < khresmoi-summary-test.tc.en > khresmoi-summary-test.bpe.en

#Prepare the training data using the MIMIC corpus translated through tagged restricted sampling
sed -e 's/^/<BT> /' $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_restricted_sampling_50.tc.de > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_restricted_sampling_50_tagged.tc.de

cd $TRAINING_WITH_TAGGED_RESTRICTED_SAMPLING_DIR

cat $UFAL_DIR/ufal_medical_without_subtitles.tc.de $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_restricted_sampling_50_tagged.tc.de > train_unshuffled.tc.de
cat $UFAL_DIR/ufal_medical_without_subtitles.tc.en $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.tc.en > train_unshuffled.tc.en

paste -d'\t' train_unshuffled.tc.de train_unshuffled.tc.en > train_unshuffled.tc.bi

shuf train_unshuffled.tc.bi > train.tc.bi

cut -f 1 train.tc.bi > train.tc.de
cut -f 2 train.tc.bi > train.tc.en

rm -f train_unshuffled*
rm -f train.tc.bi

#This time you should use the same bpe model and vocabulary learned with the untagged corpus, adding the '--glossaries' argument for not segmenting the '<BT>' tag in the source language. The dev and test corpora are also the same learned before with the untagged corpus, so there is no need to create them again.
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/bpe_model.bi --glossaries '<BT>' --vocabulary $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/vocab.de < train.tc.de > train.bpe.de
$YOUR_SUBWORD_NMT_DIR/apply_bpe.py -c $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/bpe_model.bi --vocabulary $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/vocab.en < train.tc.en > train.bpe.en

cp $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/khresmoi-summary-dev.bpe.de khresmoi-summary-dev.bpe.de
cp $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/khresmoi-summary-dev.bpe.en khresmoi-summary-dev.bpe.en
cp $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/khresmoi-summary-test.bpe.de khresmoi-summary-test.bpe.de
cp $TRAINING_WITH_RESTRICTED_SAMPLING_DIR/khresmoi-summary-test.bpe.en khresmoi-summary-test.bpe.en
