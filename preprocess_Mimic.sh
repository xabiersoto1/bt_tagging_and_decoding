#Variable definition, replace your_x_dir by your own directories.
MIMIC_DIR=your_mimic_dir
UFAL_DIR=your_ufal_dir
MOSES_TOKENIZER_DIR=your_moses_tokenizer_dir
MOSES_TRUECASER_DIR=your_moses_truecaser_dir
MOSES_CORPUS_CLEANER_DIR=your_moses_corpus_cleaner_dir
SUBWORD_NMT_DIR=your_subword_nmt_dir

cd $MIMIC_DIR

grep -v '||||' mimic-discharge.txt > mimic-discharge_without_header.txt

sed 's/\(\[\*\*\|\*\*\]\)//g' mimic-discharge_without_header.txt > mimic-discharge_without_header_without_brackets.txt

sed '/^$/d' mimic-discharge_without_header_without_brackets.txt > mimic-discharge_without_header_without_brackets_without_empty_lines.txt

$MOSES_TOKENIZER_DIR/tokenizer.perl -no-escape -a -l en < mimic-discharge_without_header_without_brackets_without_empty_lines.txt > mimic-discharge_without_header_without_brackets_without_empty_lines.tk.en

$MOSES_TRUECASER_DIR/truecase.perl --model $UFAL_DIR/truecaser.en < mimic-discharge_without_header_without_brackets_without_empty_lines.tk.en > mimic-discharge_without_header_without_brackets_without_empty_lines.tc.en

$SUBWORD_NMT_DIR/apply_bpe.py -c $UFAL_DIR/bpe_model.bi --vocabulary $UFAL_DIR/vocab.en < mimic-discharge_without_header_without_brackets_without_empty_lines.tc.en > mimic-discharge_without_header_without_brackets_without_empty_lines.bpe.en

#Duplicate the en corpus as de corpus for further corpus cleaning
cp mimic-discharge_without_header_without_brackets_without_empty_lines.bpe.en mimic-discharge_without_header_without_brackets_without_empty_lines.bpe.de

$MOSES_CORPUS_CLEANER_DIR/clean-corpus-n.perl mimic-discharge_without_header_without_brackets_without_empty_lines.bpe en de mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.bpe 1 1000
