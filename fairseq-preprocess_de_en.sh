#!/bin/bash

#Variable definition, replace your_x_dir by your own directories.
UFAL_DIR=your_ufal_dir
KHRESMOI_DIR=your_khresmoi_dir

fairseq-preprocess --source-lang de --target-lang en \
    --trainpref $UFAL_DIR/ufal_medical_without_subtitles.bpe \
    --validpref $KHRESMOI_DIR/khresmoi-summary-dev.bpe \
    --testpref $KHRESMOI_DIR/khresmoi-summary-test.bpe \
    --destdir data \
    --workers 20
