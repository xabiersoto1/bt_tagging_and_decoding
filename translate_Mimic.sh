#Variable definition, replace your_x_dir by your own directories.
MIMIC_DIR=your_mimic_dir

nohup cat $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.bpe.en | bash translate_beam_search_16.sh | grep '^H-' | sed 's/^..//' | sort -n | cut -f 3 > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_beam_search_16.bpe.de &

nohup cat $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.bpe.en | bash translate_unrestricted_sampling.sh | grep '^H-' | sed 's/^..//' | sort -n | cut -f 3 > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_unrestricted_sampling.bpe.de &

nohup cat $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned.bpe.en | bash translate_restricted_sampling.sh | grep '^H-' | sed 's/^..//' | sort -n | cut -f 3 > $MIMIC_DIR/mimic-discharge_without_header_without_brackets_without_empty_lines_corpus_cleaned_restricted_sampling_50.bpe.de &
