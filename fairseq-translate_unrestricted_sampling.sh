#!/bin/bash

export CUDA_VISIBLE_DEVICES=0

fairseq-interactive data \
    --path checkpoints/checkpoint_best.pt \
    --buffer-size 1000 \
    --sampling \
    --beam 1 \
    --nbest 1 \
    --batch-size 50 \
    --remove-bpe
