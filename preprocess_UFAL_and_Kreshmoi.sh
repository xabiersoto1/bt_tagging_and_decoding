#Variable definition, replace your_x_dir by your own directories.
UFAL_DIR=your_ufal_dir
KHRESMOI_DIR=your_khresmoi_dir
MOSES_TOKENIZER_DIR=your_moses_tokenizer_dir
MOSES_TRUECASER_DIR=your_moses_truecaser_dir
SUBWORD_NMT_DIR=your_subword_nmt_dir

#Preprocess UFAL corpus
cd $UFAL_DIR

grep 'medical_corpus' shuffled.de-en | grep -v 'Subtitles' > ufal_medical_without_subtitles.de-en

cut -f 1 ufal_medical_without_subtitles.de-en > ufal_medical_without_subtitles.de
cut -f 2 ufal_medical_without_subtitles.de-en > ufal_medical_without_subtitles.en

$MOSES_TOKENIZER_DIR/tokenizer.perl -no-escape -a -l de < ufal_medical_without_subtitles.de > ufal_medical_without_subtitles.tk.de
$MOSES_TOKENIZER_DIR/tokenizer.perl -no-escape -a -l en < ufal_medical_without_subtitles.en > ufal_medical_without_subtitles.tk.en

$MOSES_TRUECASER_DIR/train-truecaser.perl --model truecaser.de --corpus ufal_medical_without_subtitles.tk.de
$MOSES_TRUECASER_DIR/train-truecaser.perl --model truecaser.en --corpus ufal_medical_without_subtitles.tk.en

$MOSES_TRUECASER_DIR/truecase.perl --model truecaser.de < ufal_medical_without_subtitles.tk.de > ufal_medical_without_subtitles.tc.de
$MOSES_TRUECASER_DIR/truecase.perl --model truecaser.en < ufal_medical_without_subtitles.tk.en > ufal_medical_without_subtitles.tc.en

$SUBWORD_NMT_DIR/learn_joint_bpe_and_vocab.py --input ufal_medical_without_subtitles.tc.en ufal_medical_without_subtitles.tc.de -s 40000 -o bpe_model.bi --write-vocabulary vocab.en vocab.de

$SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.en < ufal_medical_without_subtitles.tc.en > ufal_medical_without_subtitles.bpe.en
$SUBWORD_NMT_DIR/apply_bpe.py -c bpe_model.bi --vocabulary vocab.de < ufal_medical_without_subtitles.tc.de > ufal_medical_without_subtitles.bpe.de

#Preprocess Khresmoi corpus
cd $KHRESMOI_DIR

$MOSES_TOKENIZER_DIR/tokenizer.perl -no-escape -a -l de < khresmoi-summary-dev.de > khresmoi-summary-dev.tk.de
$MOSES_TOKENIZER_DIR/tokenizer.perl -no-escape -a -l en < khresmoi-summary-dev.en > khresmoi-summary-dev.tk.en
$MOSES_TOKENIZER_DIR/tokenizer.perl -no-escape -a -l de < khresmoi-summary-test.de > khresmoi-summary-test.tk.de
$MOSES_TOKENIZER_DIR/tokenizer.perl -no-escape -a -l en < khresmoi-summary-test.en > khresmoi-summary-test.tk.en

$MOSES_TRUECASER_DIR/truecase.perl --model $UFAL_DIR/truecaser.de < khresmoi-summary-dev.tk.de > khresmoi-summary-dev.tc.de
$MOSES_TRUECASER_DIR/truecase.perl --model $UFAL_DIR/truecaser.en < khresmoi-summary-dev.tk.en > khresmoi-summary-dev.tc.en
$MOSES_TRUECASER_DIR/truecase.perl --model $UFAL_DIR/truecaser.de < khresmoi-summary-test.tk.de > khresmoi-summary-test.tc.de
$MOSES_TRUECASER_DIR/truecase.perl --model $UFAL_DIR/truecaser.en < khresmoi-summary-test.tk.en > khresmoi-summary-test.tc.en

$SUBWORD_NMT_DIR/apply_bpe.py -c $UFAL_DIR/bpe_model.bi --vocabulary $UFAL_DIR/vocab.en < khresmoi-summary-dev.tc.en > khresmoi-summary-dev.bpe.en
$SUBWORD_NMT_DIR/apply_bpe.py -c $UFAL_DIR/bpe_model.bi --vocabulary $UFAL_DIR/vocab.de < khresmoi-summary-dev.tc.de > khresmoi-summary-dev.bpe.de
$SUBWORD_NMT_DIR/apply_bpe.py -c $UFAL_DIR/bpe_model.bi --vocabulary $UFAL_DIR/vocab.en < khresmoi-summary-test.tc.en > khresmoi-summary-test.bpe.en
$SUBWORD_NMT_DIR/apply_bpe.py -c $UFAL_DIR/bpe_model.bi --vocabulary $UFAL_DIR/vocab.de < khresmoi-summary-test.tc.de > khresmoi-summary-test.bpe.de
