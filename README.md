# bt_tagging_and_decoding

In this repository you can find the scripts for reproducing the de-en experiments presented in the paper "Comparing and combining tagging with different decoding algorithms for back-translation in NMT: learnings from a low resource scenario", accepted for publication in EAMT 2022.

If you use this code, please cite as:

```bibtex
@inproceedings{soto2022comparing,
  title = {Comparing and combining tagging with different decoding algorithms for back-translation in NMT: learnings from a low resource scenario},
  author = {Xabier Soto and Olatz Perez-de-Viñaspre and Gorka Labaka and Maite Oronoz},
  booktitle = {Proceedings of the 23rd Annual Conference of the European Association for Machine Translation (EAMT 2022): Research track},
  year = {2022}
}
```

In the following, we present some guidelines for reproducing these experiments:

# Install fairseq

Clone the repository here (we used version 0.9.0): https://github.com/pytorch/fairseq

# Download the corpora

Download UFAL corpus from here (notice that you'll need to register first): https://ufal.mff.cuni.cz/ufal_medical_corpus

Download Khresmoi corpus from here: https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-2122

Download Mimic corpus following the instructions here: https://mimic.physionet.org/gettingstarted/access/

# Download the tools for preprocessing the corpora

Tokenizer: https://github.com/moses-smt/mosesdecoder/blob/master/scripts/tokenizer/tokenizer.perl

Truecaser: https://github.com/moses-smt/mosesdecoder/blob/master/scripts/recaser/truecase.perl

Corpus cleaning: https://github.com/moses-smt/mosesdecoder/blob/master/scripts/training/clean-corpus-n.perl

Word segmentation: https://github.com/rsennrich/subword-nmt/tree/71b22d1a99a1c22be95c661c02372700d841124e

# Define the variables according to your local directories

Name the variables presented in the beginning of each script, replacing 'your_x_dir' by the corresponding directory of your downloaded corpus / preprocessing tool.

# Preprocess

Preprocess the UFAL, Khresmoi and Mimic corpora by running the corresponding scripts ('preprocess_x.sh').

# Train the system for bt

Train the en-de system by running the corresponding scripts for preprocessing ('fairseq-preprocess_en_de.sh') and training ('fairseq-train.sh').

# Select the best model for bt

For each of the models saved after every epoch, calculate the BLEU scores on the validation set by running the fairseq-generate script, and select the one that obtains the highest BLEU score, naming it 'checkpoint_best.pt'.

# Translate the monolingual corpus

Translate Mimic corpus by the en-de system using different decoding algorithms (beam search, unrestricted sampling and restricted sampling), by running the script 'translate_Mimic.sh'.

# Train the final systems

Concatenate the bilingual corpus from UFAL to the synthetic corpora created by each of the different decoding algorithms, using a tag or not.

Train the 6 de-en systems by running the corresponding scripts for preprocessing ('fairseq-preprocess_de_en.sh') and training ('fairseq-train.sh').

# Select the best model for the final systems

For each of the 6 de-en systems, and each of the models saved after every epoch, calculate the BLEU scores on the validation set by running the fairseq-generate script, and select the one that obtains the highest BLEU score for each system.

# Test the final systems

Evaluate on the test set each of the 6 de-en systems that obtained the highest BLEU score on the validation set.

In case of any doubt, contact the first author at: xabier.soto@ehu.eus
