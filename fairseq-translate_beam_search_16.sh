#!/bin/bash

export CUDA_VISIBLE_DEVICES=0

fairseq-interactive data \
    --path checkpoints/checkpoint_best.pt \
    --buffer-size 1024 \
    --beam 16 \
    --batch-size 16 \
    --remove-bpe
